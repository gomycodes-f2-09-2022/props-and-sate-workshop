import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

export default function Header({title,username}) {
  return (
    <Navbar bg="dark" variant="dark">
        <Container>
        <Navbar.Brand href="#home">{title}</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
            Signed in as: <a href="#login">{username}</a>
            </Navbar.Text>
        </Navbar.Collapse>
        </Container>
    </Navbar>
  )
}
