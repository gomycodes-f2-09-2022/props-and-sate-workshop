import React from 'react';
import Table from 'react-bootstrap/Table';

const List = ({width, products}) => {

    return (
        <div style={{width: `${width}%`}}>
        <Table striped bordered hover>
        <thead>
            <tr>
            <th>#</th>
            <th>Name</th>
            <th>Description</th>
            </tr>
        </thead>
        <tbody>
            {
                products && products.length ? 
                products.map((product, key) => (
                    <tr key={key}>
                    <td>{key}</td>
                    <td>{product.name}</td>
                    <td>{product.description}</td>
                    </tr>
                )) 
                : 
                (<tr><td row='3'></td></tr>)
            }
        </tbody>
        </Table>
        </div>
    );
}

export default List;
