import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header/Header';
import List from './List/List';
import Form1 from './Form/Form';

function App() {

  const [products, setProducts] = useState([]);

  function updateProduct(product){
    setProducts([...products, product]);
  }
  return (
    <div className="App">
      <Header title={"E-commerce"} username={"Aikpé Achile"}></Header>
      <div style={{display: 'flex', justifyContent: 'space-around'}}>
        <Form1 width={28} updateProduct={updateProduct}></Form1>
        <List width={70} products={products}></List>
      </div>
    </div>
  );
}

export default App;
