import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useState } from 'react';

const Form1 = ({width, updateProduct}) => {

    const [name,setName] = useState('');
    const [description,setDescription] = useState('');
    
    function addProduct(e){
        e.preventDefault();
        updateProduct({name, description})
    }
    return (
        <div style={{width: `${width}%`}}>
            <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label>Nom</Form.Label>
                <Form.Control type="text" placeholder="nom" onChange={(e) => setName(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                <Form.Label>Description</Form.Label>
                <Form.Control as="textarea" rows={3} onChange={(e) => setDescription(e.target.value)} />
            </Form.Group>
            <Button onClick={addProduct} variant="primary">Primary</Button>{' '}
            </Form>
        </div>
    );
}

export default Form1;
